package br.pucpr.appdev.listacompras.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Lista implements Serializable {

    private String id;
    private String name;
    private String user;
    private int numItems = 0;
    private int checkedItems = 0;
    private boolean shared = false;

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public int getCheckedItems() {
        return checkedItems;
    }

    public void setCheckedItems(int checkedItems) {
        this.checkedItems = checkedItems;
    }

    public int getNumItems() {
        return numItems;
    }

    public void setNumItems(int numItems) {
        this.numItems = numItems;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("user", user);
        map.put("num_items", numItems);
        map.put("checked_items", checkedItems);
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lista lista = (Lista) o;
        return Objects.equals(id, lista.id) &&
                Objects.equals(name, lista.name) &&
                Objects.equals(user, lista.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, user);
    }
}
