package br.pucpr.appdev.listacompras.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import br.pucpr.appdev.listacompras.R;
import br.pucpr.appdev.listacompras.adapter.ItemListAdapter;
import br.pucpr.appdev.listacompras.model.Item;
import br.pucpr.appdev.listacompras.model.Lista;

public class ListItemsActivity extends AppCompatActivity {

    RecyclerView rvItemsList;
    FirebaseFirestore fFirestore;
    ItemListAdapter adapter;
    Lista lista;
    FirebaseAnalytics fAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_items);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        rvItemsList = findViewById(R.id.itemsList);
        rvItemsList.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvItemsList.getContext(), layoutManager.getOrientation());
        rvItemsList.addItemDecoration(dividerItemDecoration);

        fFirestore = FirebaseFirestore.getInstance();
        fAnalytics = FirebaseAnalytics.getInstance(this);

        lista = (Lista) getIntent().getSerializableExtra("lista");
        setupData(lista);

        setTitle(lista.getName());

        setupFAB();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return true;
    }

    public void setupData(final Lista lista) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Carregando...");
        pd.show();

        fFirestore.collection("listas").document(lista.getId()).collection("items").get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<Item> items = new ArrayList<>();

                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    Item i = new Item();
                    i.setId(documentSnapshot.getId());
                    i.setName(documentSnapshot.getString("name"));
                    i.setChecked(documentSnapshot.getBoolean("checked"));
                    i.setQuantity(documentSnapshot.getLong("quantity").intValue());
                    i.setValue(documentSnapshot.getDouble("value").floatValue());
                    i.setListaId(lista.getId());
                    items.add(i);
                }

                if (items.isEmpty()) {
                    findViewById(R.id.empty_item_list).setVisibility(View.VISIBLE);
                    rvItemsList.setVisibility(View.GONE);
                }

                adapter = new ItemListAdapter(ListItemsActivity.this, items);
                rvItemsList.setAdapter(adapter);
                pd.dismiss();
            }
        });
    }

    public void setupFAB() {
        FloatingActionButton fab = findViewById(R.id.fab_items);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(ListItemsActivity.this);
                dialog.setContentView(R.layout.form_item_list);

                final TextInputEditText name = dialog.findViewById(R.id.form_item_list_name);
                final TextInputEditText quantity = dialog.findViewById(R.id.form_item_list_quantity);
                final TextInputEditText value = dialog.findViewById(R.id.form_item_list_value);
                Button cancelarBtn = dialog.findViewById(R.id.cancel_form_item_list);
                Button saveBtn = dialog.findViewById(R.id.save_form_item_list);
                final TextView textCounter = dialog.findViewById(R.id.form_item_list_counter);

                name.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                        textCounter.setText((50 - charSequence.length()) + " caractéres restantes");
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                cancelarBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });

                saveBtn.setOnClickListener(new View.OnClickListener() {
                    private boolean validation() {
                        boolean result = true;

                        if (name.getText().toString().isEmpty()) {
                            name.setError("Preencha o campo nome");
                            result = false;
                        }

                        if (value.getText().toString().isEmpty()) {
                            value.setError("Adicione um valor a este item");
                            result = false;
                        }

                        return result;
                    }

                    @Override
                    public void onClick(View view) {
                        if (validation()) {
                            final ProgressDialog pd = new ProgressDialog(ListItemsActivity.this);
                            pd.setTitle("Salvando...");
                            pd.show();
                            final Item item = new Item();

                            int quantityFormated = 1;

                            if (!quantity.getText().toString().isEmpty()) {
                                quantityFormated = Integer.valueOf(quantity.getText().toString());
                                if (quantityFormated <= 0)
                                    quantityFormated = 1;
                            }

                            item.setName(name.getText().toString());
                            item.setValue(Float.parseFloat(value.getText().toString()));
                            item.setQuantity(quantityFormated);
                            item.setChecked(false);
                            item.setListaId(lista.getId());
                            fFirestore.collection("listas").document(lista.getId()).collection("items").add(item.toMap())
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(final DocumentReference documentReference) {
                                            fFirestore.collection("listas").document(lista.getId()).get()
                                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                            int actual = documentSnapshot.getLong("num_items").intValue();
                                                            documentSnapshot.getReference().update("num_items", actual + 1);
                                                        }
                                                    });
                                            item.setId(documentReference.getId());
                                            adapter.addNewItem(item);

                                            Bundle b = new Bundle();
                                            b.putString("new_item_name", item.getName());
                                            fAnalytics.logEvent("new_item", b);

                                            if (adapter.isEmpty()) {
                                                findViewById(R.id.empty_item_list).setVisibility(View.GONE);
                                                rvItemsList.setVisibility(View.VISIBLE);
                                            }

                                            dialog.dismiss();
                                            pd.dismiss();
                                        }
                                    });
                        }
                    }
                });

                dialog.show();
            }
        });
    }


}
