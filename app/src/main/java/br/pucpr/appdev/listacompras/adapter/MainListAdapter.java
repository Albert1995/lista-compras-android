package br.pucpr.appdev.listacompras.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import br.pucpr.appdev.listacompras.view.ListItemsActivity;
import br.pucpr.appdev.listacompras.R;
import br.pucpr.appdev.listacompras.model.Lista;
import br.pucpr.appdev.listacompras.view.MainActivity;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.MainListViewHolder> {

    private List<Lista> listas;
    private Context ctx;
    private FirebaseFirestore fFirestore;

    public MainListAdapter(Context ctx, List<Lista> listas) {
        this.ctx = ctx;
        this.listas = listas;
        fFirestore = FirebaseFirestore.getInstance();
    }

    public void addNewItem(Lista l) {
        listas.add(l);
        notifyDataSetChanged();
    }

    public void removeItem(Lista l) {
        listas.remove(l);
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return listas.isEmpty();
    }

    @NonNull
    @Override
    public MainListViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_list_item, parent, false);

        final MainListViewHolder viewHolder = new MainListViewHolder(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, ListItemsActivity.class);
                i.putExtra("lista", listas.get(viewHolder.getAdapterPosition()));
                ctx.startActivity(i);
            }
        });

        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final Dialog d = new Dialog(ctx);
                d.setContentView(R.layout.form_share_list);

                final TextInputEditText userEmail = d.findViewById(R.id.user_share_email);
                final RecyclerView rvSharedUsers = d.findViewById(R.id.users_shared_list);
                Button addButton = d.findViewById(R.id.share_add_button);
                Button close = d.findViewById(R.id.close_button_share);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                    }
                });

                rvSharedUsers.setLayoutManager(new LinearLayoutManager(d.getContext()));

                fFirestore.collection("listas").document(listas.get(viewHolder.getAdapterPosition()).getId()).get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                List<String> users = (List<String>) documentSnapshot.get("shared_with");

                                if (users == null)
                                    users = new ArrayList<>();

                                rvSharedUsers.setAdapter(new SharedUsersAdapter(documentSnapshot.getId(), users));
                            }
                        });


                addButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fFirestore.collection("users")
                                .whereEqualTo("email", userEmail.getText().toString())
                                .whereEqualTo("active", true).get()
                                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                    @Override
                                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                        if (queryDocumentSnapshots.getDocuments().size() > 0) {
                                            final String userId = queryDocumentSnapshots.getDocuments().get(0).getId();

                                            if (userId.equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                                userEmail.setError("Você não precisa compartilhar para você mesmo");
                                                return;
                                            }

                                            fFirestore.collection("listas").document(listas.get(viewHolder.getAdapterPosition()).getId()).get()
                                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                        @Override
                                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                            List<String> users = (List<String>) documentSnapshot.get("shared_with");

                                                            if (users == null) {
                                                                users = new ArrayList<>();
                                                            }

                                                            users.add(userId);
                                                            documentSnapshot.getReference().update("shared_with", users);
                                                            ((SharedUsersAdapter) rvSharedUsers.getAdapter()).setUsers(users);
                                                            userEmail.setText("");
                                                        }
                                                    });
                                        } else {
                                            userEmail.setError("Nenhum usuário encontrado");
                                        }
                                    }
                                });
                    }
                });
                d.show();

                return true;
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MainListViewHolder holder, int position) {
        holder.listName.setText(listas.get(position).getName());
        holder.progressList.setText(listas.get(position).getCheckedItems() + " de " + listas.get(position).getNumItems() + " comprados" + (listas.get(position).isShared() ? " (Compartilhada)" : ""));
        holder.deleteButton.setVisibility(listas.get(position).isShared() ? View.GONE : View.VISIBLE);
        holder.editButton.setVisibility(listas.get(position).isShared() ? View.GONE : View.VISIBLE);

        if (listas.get(position).isShared())
            holder.itemView.setOnLongClickListener(null);
    }

    @Override
    public int getItemCount() {
        return listas.size();
    }

    class MainListViewHolder extends RecyclerView.ViewHolder {

        TextView listName, progressList;
        ImageButton editButton;
        ImageButton deleteButton;

        public MainListViewHolder(@NonNull View itemView) {
            super(itemView);

            listName = itemView.findViewById(R.id.main_list_item_name);
            progressList = itemView.findViewById(R.id.main_list_item_progress);
            editButton = itemView.findViewById(R.id.main_list_edit);
            deleteButton = itemView.findViewById(R.id.main_list_delete);



            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Lista l = listas.get(getAdapterPosition());

                    AlertDialog.Builder deleteAlert = new AlertDialog.Builder(ctx);
                    deleteAlert.setTitle("Você tem certeza?");
                    deleteAlert.setMessage("Você está excluindo a lista \"" + l.getName() + "\". Esta não pode ser desfeita.");
                    deleteAlert.setNegativeButton("Não", null);
                    deleteAlert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final ProgressDialog pd = new ProgressDialog(ctx);
                            pd.setTitle("Excluindo...");
                            pd.show();

                            fFirestore.collection("listas").document(l.getId()).collection("items").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                    for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()) {
                                        doc.getReference().delete();
                                    }

                                    fFirestore.collection("listas").document(l.getId()).delete();
                                    removeItem(l);
                                    pd.dismiss();
                                }
                            });
                        }
                    });
                    deleteAlert.show();
                }
            });

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Lista l = listas.get(getAdapterPosition());
                    final Dialog dialog = new Dialog(ctx);
                    dialog.setContentView(R.layout.form_main_list);

                    final EditText nameList = dialog.findViewById(R.id.form_main_list_name);
                    final TextView textCounter = dialog.findViewById(R.id.form_main_list_counter);
                    Button cancelarBtn = dialog.findViewById(R.id.cancel_form_main_list);
                    Button saveBtn = dialog.findViewById(R.id.save_form_main_list);

                    nameList.setText(l.getName());
                    textCounter.setText((50 - l.getName().length()) + " caractéres restantes");
                    nameList.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                            textCounter.setText((50 - charSequence.length()) + " caractéres restantes");
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });


                    cancelarBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.cancel();
                        }
                    });

                    saveBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final ProgressDialog pd = new ProgressDialog(ctx);
                            pd.setTitle("Alterando...");
                            pd.show();
                            l.setName(nameList.getText().toString());

                            fFirestore.collection("listas").document(l.getId()).update(l.toMap()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    MainListAdapter.this.notifyDataSetChanged();
                                    pd.dismiss();
                                    dialog.dismiss();
                                }
                            });
                        }
                    });
                    dialog.show();
                }
            });
        }

    }


}
