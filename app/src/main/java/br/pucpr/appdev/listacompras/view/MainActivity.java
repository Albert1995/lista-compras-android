package br.pucpr.appdev.listacompras.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.pucpr.appdev.listacompras.R;
import br.pucpr.appdev.listacompras.adapter.MainListAdapter;
import br.pucpr.appdev.listacompras.model.Lista;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView rvMainList;
    MainListAdapter adapter;
    LinearLayoutManager layoutManager;
    FirebaseFirestore fFirestore;
    FirebaseAnalytics fAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        fAnalytics = FirebaseAnalytics.getInstance(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.form_main_list);

                final TextInputEditText nameList = dialog.findViewById(R.id.form_main_list_name);
                Button cancelarBtn = dialog.findViewById(R.id.cancel_form_main_list);
                Button saveBtn = dialog.findViewById(R.id.save_form_main_list);
                final TextView textCounter = dialog.findViewById(R.id.form_main_list_counter);

                nameList.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                        textCounter.setText((50 - charSequence.length()) + " caractéres restantes");
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                cancelarBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });

                saveBtn.setOnClickListener(new View.OnClickListener() {
                    private boolean validation() {
                        boolean result = true;

                        if (nameList.getText().toString().isEmpty()) {
                            result = false;
                            nameList.setError("O nome da lista não pode estar vazio!");
                        }

                        return result;
                    }

                    @Override
                    public void onClick(View view) {
                        if (validation()) {
                            final ProgressDialog pd = new ProgressDialog(MainActivity.this);
                            pd.setTitle("Criando...");
                            pd.show();
                            final Lista l = new Lista();
                            l.setName(nameList.getText().toString());
                            l.setUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            fFirestore.collection("listas").add(l.toMap()).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    l.setId(documentReference.getId());
                                    adapter.addNewItem(l);

                                    Bundle b = new Bundle();
                                    b.putString("new_list_name", l.getName());
                                    fAnalytics.logEvent("new_list", b);

                                    if (!adapter.isEmpty()) {
                                        findViewById(R.id.empty_main_list).setVisibility(View.GONE);
                                        rvMainList.setVisibility(View.VISIBLE);
                                    }

                                    pd.dismiss();
                                    dialog.dismiss();
                                }
                            });
                        }
                    }
                });

                dialog.show();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        fFirestore = FirebaseFirestore.getInstance();

        setupRecyclerView();
        setupData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupData();
    }

    private void setupRecyclerView() {
        rvMainList = findViewById(R.id.main_list);

        layoutManager = new LinearLayoutManager(this);
        rvMainList.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvMainList.getContext(), layoutManager.getOrientation());
        rvMainList.addItemDecoration(dividerItemDecoration);
    }

    private void setupData() {
        final ProgressDialog pd = new ProgressDialog(MainActivity.this);
        pd.setTitle("Atualizando...");
        pd.show();
        fFirestore.collection("listas").whereEqualTo("user", FirebaseAuth.getInstance().getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        final List<Lista> listas = new ArrayList<>();

                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            Lista l = new Lista();
                            l.setId(documentSnapshot.getId());
                            l.setName(documentSnapshot.getString("name"));
                            l.setUser(documentSnapshot.getString("user"));
                            l.setNumItems(documentSnapshot.getLong("num_items").intValue());
                            l.setCheckedItems(documentSnapshot.getLong("checked_items").intValue());
                            listas.add(l);
                        }

                        if (listas.isEmpty()) {
                            findViewById(R.id.empty_main_list).setVisibility(View.VISIBLE);
                            rvMainList.setVisibility(View.GONE);
                        }

                        fFirestore.collection("listas").whereArrayContains("shared_with", FirebaseAuth.getInstance().getCurrentUser().getUid()).get()
                                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                    @Override
                                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                            Lista l = new Lista();
                                            l.setId(documentSnapshot.getId());
                                            l.setName(documentSnapshot.getString("name"));
                                            l.setUser(documentSnapshot.getString("user"));
                                            l.setNumItems(documentSnapshot.getLong("num_items").intValue());
                                            l.setCheckedItems(documentSnapshot.getLong("checked_items").intValue());
                                            l.setShared(true);
                                            listas.add(l);
                                        }

                                        adapter = new MainListAdapter(MainActivity.this, listas);
                                        rvMainList.setAdapter(adapter);
                                        pd.dismiss();
                                    }
                                });


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("retrie-main-lists-fail", e.getMessage());
                        pd.dismiss();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(this, ProfileActivity.class));
        } else if (id == R.id.nav_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
