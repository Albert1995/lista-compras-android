package br.pucpr.appdev.listacompras.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import br.pucpr.appdev.listacompras.R;

public class LoginActivity extends AppCompatActivity {

    EditText email, password;
    FirebaseAuth fAuth;
    TextView wrongMsg;
    FirebaseAnalytics fAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.login_email);
        password = findViewById(R.id.login_password);
        wrongMsg = findViewById(R.id.wrong_login_msg);

        fAuth = FirebaseAuth.getInstance();
        fAnalytics = FirebaseAnalytics.getInstance(this);

        if (fAuth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            this.finish();
        }
    }

    public void registrarOnClick(View v) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    public void entrarOnClick(View v) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Autenticando...");
        pd.show();
        fAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.METHOD, "EMAIL");
                        fAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("login-fail", e.getMessage());
                        wrongMsg.setVisibility(View.VISIBLE);

                    }
                })
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        pd.dismiss();
                    }
                });
    }





}
