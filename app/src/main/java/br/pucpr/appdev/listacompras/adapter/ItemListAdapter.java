package br.pucpr.appdev.listacompras.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import br.pucpr.appdev.listacompras.R;
import br.pucpr.appdev.listacompras.model.Item;
import br.pucpr.appdev.listacompras.model.Lista;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ItemListViewHolder> {

    private Context ctx;
    private List<Item> lista;
    private FirebaseFirestore fFirestore;

    public ItemListAdapter(Context ctx, List<Item> lista) {
        this.ctx = ctx;
        this.lista = lista;
        fFirestore = FirebaseFirestore.getInstance();
    }

    public void addNewItem(Item i) {
        lista.add(i);
        notifyDataSetChanged();
    }

    public void removeItem(Item i) {
        lista.remove(i);
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return lista.isEmpty();
    }

    @NonNull
    @Override
    public ItemListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_item, parent, false);

        ItemListViewHolder viewHolder = new ItemListViewHolder(itemView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemListViewHolder holder, int position) {
        Item i = lista.get(position);

        holder.name.setText(i.getName());
        holder.quantity.setText(String.valueOf(i.getQuantity()));
        holder.value.setText(String.format("%.2f", i.getValue()));
        holder.total.setText(String.format("%.2f", i.getValue() * i.getQuantity()));
        holder.checkBox.setChecked(i.isChecked());
        holder.setCheckboxListener();
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    class ItemListViewHolder extends RecyclerView.ViewHolder {

        TextView name, value, total, quantity;
        ImageButton editButton;
        ImageButton deleteButton;
        CheckBox checkBox;

        public ItemListViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_list_item_name);
            editButton = itemView.findViewById(R.id.item_list_edit);
            deleteButton = itemView.findViewById(R.id.item_list_delete);
            value = itemView.findViewById(R.id.item_list_item_value);
            total = itemView.findViewById(R.id.item_list_item_total);
            quantity = itemView.findViewById(R.id.item_list_item_quant);
            checkBox = itemView.findViewById(R.id.item_list_item_check);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Item i = lista.get(getAdapterPosition());

                    AlertDialog.Builder deleteAlert = new AlertDialog.Builder(ctx);
                    deleteAlert.setTitle("Você tem certeza?");
                    deleteAlert.setMessage("Você está excluindo o item \"" + i.getName() + "\". Esta não pode ser desfeita.");
                    deleteAlert.setNegativeButton("Não", null);
                    deleteAlert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i2) {
                            final ProgressDialog pd = new ProgressDialog(ctx);
                            pd.setTitle("Excluindo...");
                            pd.show();

                            fFirestore.collection("listas").document(i.getListaId()).collection("items").document(i.getId()).delete()
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            fFirestore.collection("listas").document(i.getListaId()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                @Override
                                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                    int actualItems = documentSnapshot.getLong("num_items").intValue();
                                                    int actualChecked = documentSnapshot.getLong("checked_items").intValue();
                                                    documentSnapshot.getReference().update("num_items", actualItems - 1);
                                                    if (i.isChecked())
                                                        documentSnapshot.getReference().update("checked_items", actualChecked - 1);
                                                }
                                            });
                                            removeItem(i);
                                            pd.dismiss();
                                        }
                                    });
                        }
                    });
                    deleteAlert.show();
                }
            });

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Item i = lista.get(getAdapterPosition());
                    final Dialog dialog = new Dialog(ctx);
                    dialog.setContentView(R.layout.form_item_list);

                    final EditText name = dialog.findViewById(R.id.form_item_list_name);
                    final EditText quatity = dialog.findViewById(R.id.form_item_list_quantity);
                    final EditText value = dialog.findViewById(R.id.form_item_list_value);
                    final Button cancelarBtn = dialog.findViewById(R.id.cancel_form_item_list);
                    Button saveBtn = dialog.findViewById(R.id.save_form_item_list);

                    name.setText(i.getName());
                    quatity.setText(String.valueOf(i.getQuantity()));
                    value.setText(String.valueOf(i.getValue()));

                    cancelarBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.cancel();
                        }
                    });

                    saveBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final ProgressDialog pd = new ProgressDialog(ctx);
                            pd.setTitle("Atualizando...");
                            pd.show();
                            i.setName(name.getText().toString());
                            i.setQuantity(Integer.valueOf(quatity.getText().toString()));
                            i.setValue(Float.valueOf(value.getText().toString()));

                            fFirestore.collection("listas").document(i.getListaId()).collection("items").document(i.getId()).update(i.toMap()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    ItemListAdapter.this.notifyDataSetChanged();
                                    dialog.dismiss();
                                    pd.dismiss();
                                }
                            });
                        }
                    });
                    dialog.show();
                }
            });


        }

        public void setCheckboxListener() {
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                    final Item i = lista.get(getAdapterPosition());
                    fFirestore.collection("listas").document(i.getListaId()).collection("items").document(i.getId()).update("checked", b)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    fFirestore.collection("listas").document(i.getListaId()).get()
                                            .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                @Override
                                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                    int actual = documentSnapshot.getLong("checked_items").intValue();
                                                    documentSnapshot.getReference().update("checked_items", actual + (b ? 1 : -1));
                                                }
                                            });
                                }
                            });

                }
            });
        }
    }
}
