package br.pucpr.appdev.listacompras.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import br.pucpr.appdev.listacompras.R;

public class SignUpActivity extends AppCompatActivity {

    TextInputEditText email, password, passwordValidation, name, birthdate;
    FirebaseAuth fAuth;
    FirebaseFirestore fFirestore;
    FirebaseAnalytics fAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        email = findViewById(R.id.sign_up_email);
        password = findViewById(R.id.sign_up_password);
        passwordValidation = findViewById(R.id.sign_up_password_validation);
        name = findViewById(R.id.sign_up_name);
        birthdate = findViewById(R.id.sign_up_birthdate);

        fAuth = FirebaseAuth.getInstance();
        fFirestore = FirebaseFirestore.getInstance();
        fAnalytics = FirebaseAnalytics.getInstance(this);

        birthdate.setEnabled(false);
    }

    public void cancelarOnClick(View v) {
        finish();
    }

    public void registrarOnClick(View v) {
        if (validate()) {
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setTitle("Criando o usuário");
            pd.show();
            fAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            Map<String, Object> user = new HashMap<>();
                            user.put("name", name.getText().toString());
                            try {
                                user.put("birthdate", new SimpleDateFormat("dd/MM/yyyy").parse(birthdate.getText().toString()));
                            } catch (ParseException e) { }
                            user.put("email", email.getText().toString());
                            fFirestore.collection("users").document(authResult.getUser().getUid()).set(user);
                            Toast.makeText(SignUpActivity.this, "Usuário criado", Toast.LENGTH_SHORT);

                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.METHOD, "EMAIL");
                            fAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);

                            SignUpActivity.this.finish();
                            pd.dismiss();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("sign-up-error", e.getMessage());
                            pd.dismiss();
                        }
                    });
        }
    }

    public void setBirthDate(View v) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this);
        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                birthdate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            }
        });
        datePickerDialog.show();
    }

    private boolean validate() {
        boolean validation = true;

        if (name.getText().toString().isEmpty()) {
            name.setError("Preencha com o seu nome");
            validation = false;
        }

        if (password.getText().toString().isEmpty()) {
            email.setError("Preencha com o seu e-mail");
            validation = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).find()) {
            Log.d("email-invalid", "E-mail invalido");
            email.setError("E-Mail invalido");
            validation = false;
        }

        if (birthdate.getText().toString().isEmpty()) {
            birthdate.setError("Escolha a data de nascimento");
            validation = false;
        }

        if (password.getText().toString().length() < 4) {
            password.setError("A senha deve haver pelo menos 4 caracteres");
            validation = false;
        }

        if (!password.getText().toString().equals(passwordValidation.getText().toString())) {
            Log.d("password-not-validated", "Senha e Confirmação de senha são diferentes");
            passwordValidation.setError("Senha e confirmação de senha estão diferentes");
            validation = false;
        }

        return validation;
    }


}
