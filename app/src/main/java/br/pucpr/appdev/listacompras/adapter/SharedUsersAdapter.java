package br.pucpr.appdev.listacompras.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import br.pucpr.appdev.listacompras.R;

public class SharedUsersAdapter extends RecyclerView.Adapter<SharedUsersAdapter.SharedUsersViewHolder> {

    FirebaseFirestore firestore;
    List<String> users;
    String listaId;

    public SharedUsersAdapter(String listaId, List<String> users) {
        firestore = FirebaseFirestore.getInstance();

        this.listaId = listaId;
        this.users = users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SharedUsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_shared_item, parent, false);

        final SharedUsersViewHolder viewHolder = new SharedUsersViewHolder(itemView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SharedUsersViewHolder holder, int position) {
        firestore.collection("users").document(users.get(position)).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                holder.name.setText(documentSnapshot.getString("name"));
            }
        });

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class SharedUsersViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageButton deleteButton;
        ImageButton close;

        public SharedUsersViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.share_user_name_item);
            deleteButton = itemView.findViewById(R.id.delete_user_share);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    users.remove(getAdapterPosition());
                    firestore.collection("listas").document(listaId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            documentSnapshot.getReference().update("shared_with", users);
                            SharedUsersAdapter.this.notifyDataSetChanged();
                        }
                    });
                }
            });
        }
    }

}
